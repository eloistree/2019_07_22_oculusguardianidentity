﻿using UnityEngine.Events;

[System.Serializable]
public class OnQuestGuaridanChange : UnityEvent<QuestGuardian> { }