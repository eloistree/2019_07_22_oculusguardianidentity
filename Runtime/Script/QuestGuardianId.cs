﻿using System;

[System.Serializable]
public class QuestGuardianId
{
    public int m_anchorsCount;
    public int m_maxBorderDistanceInMm;
    public static QuestGuardianId CreateFrom(QuestGuardian guardian)
    {
        return new QuestGuardianId() { m_anchorsCount = guardian.m_border.m_points.Length, m_maxBorderDistanceInMm = (int)(guardian.m_border.m_segmentdistances.m_totaleDistance * 1000f) };
    }

    public string GetIdAsString()
    {
        return m_anchorsCount + "_" + m_maxBorderDistanceInMm;
    }
    public static QuestGuardianId CreateFromStringId(string id)
    {
        string[] tokens = id.Split('_');
        if (tokens.Length != 2) return null;
        try
        {
            return new QuestGuardianId() { m_anchorsCount = int.Parse(tokens[0]), m_maxBorderDistanceInMm = int.Parse(tokens[0]) };
        }
        catch (Exception) { }
        return null;
    }
}
