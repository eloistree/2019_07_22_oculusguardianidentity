﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class GuardianArchive : MonoBehaviour
{
    public bool m_saveInPlayerPrefabs=true;
    public bool m_saveInQuestFolder=true;
    public string m_questFolder = "GuardianArchive";
    public string[] m_guardianRegistreredFolder = new string[] { };
    public string[] m_guardianRegistreredPlayPref = new string[] { };
    public QuestGuardian m_loadFromFiles;

    public void RegisterGuardian(QuestGuardian guardian)
    {
        string guardianId = guardian.GetId().GetIdAsString();
        string guardianText = guardian.ToText();
        if (m_saveInQuestFolder)
        {
            string folderPath = GetGuaridanStoragePath(m_questFolder);
            string filePath = folderPath + "/" + guardian.GetId().GetIdAsString() + ".json";
            Debug.Log(filePath);

            Directory.CreateDirectory(folderPath);
            File.WriteAllText(filePath, guardianText);
            CheckGuardianSaved();
            m_loadFromFiles = QuestGuardian.FromText(File.ReadAllText(filePath));
            Debug.Log(">Load: " + m_loadFromFiles.GetId().GetIdAsString());
        }
        if (m_saveInPlayerPrefabs)
        {
            AddGuardianIdToPlayerPrefs(guardianId);
            SaveGuardianInPlayerPref(guardianId, guardianText);

        }

    }

    private void AddGuardianIdToPlayerPrefs(string guardianId)
    {
        List<string> registeredList = GetRegisteredGuaridanIdInPlayerPrefs();
        registeredList.Add(guardianId);
        m_guardianRegistreredPlayPref = registeredList.ToArray();
        PlayerPrefs.SetString("GuardianIdArchived", string.Join(",", m_guardianRegistreredPlayPref));
    }

    public static List<string> GetRegisteredGuaridanIdInPlayerPrefs()
    {
        string ids = PlayerPrefs.GetString("GuardianIdArchived", "");
        return ids.Split(',').ToList();
    }

    private static void SaveGuardianInPlayerPref(string guardianId, string guardianText)
    {
        PlayerPrefs.SetString("G:" + guardianId, guardianText);
    }

    public static string GetRegisteredGuardianInPlayerPref(string id)
    {
        return PlayerPrefs.GetString("G:" + id, ""); 
    }
    public static QuestGuardian CreateGuardianFromPlayerPref(string id) {
        return QuestGuardian.FromText(GetRegisteredGuardianInPlayerPref(id));
    }

    public static string GetGuaridanStoragePath(string questFolder)
    {
        string folderPath = "";

        if (Application.platform == RuntimePlatform.Android)
        {
            folderPath = "/" + questFolder;
        }
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            folderPath = Application.persistentDataPath + "/" + questFolder;
        }
#if UNITY_EDITOR
        folderPath = Application.dataPath + "/" + questFolder;
#endif
        return folderPath;
    }

    public void OnValidate()
    {
        CheckGuardianSaved();
    }

    private void CheckGuardianSaved()
    {
        string pathFolder = GetGuaridanStoragePath(m_questFolder);
        if (Directory.Exists(pathFolder))
            m_guardianRegistreredFolder  =GetGuardianFiles(pathFolder);
    }

    private string [] GetGuardianFiles(string pathFolder)
    {
        List<string> list = Directory.GetFiles(pathFolder).ToList();
        for (int i = list.Count-1; i >= 0; i--)
        {
            if (list[i].IndexOf(".meta") > -1)
                list.RemoveAt(i);
        }
        return list.ToArray();
    }
}
