﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGuardianFromTransformsMono : MonoBehaviour
{
    public Transform m_root;
    public Transform[] m_lineBorder;
    public Transform[] m_squareArea;

    public QuestGuardian m_guardianValue;
    public OnQuestGuaridanChange m_onRefreshed;
    private void Start()
    {
        m_onRefreshed.Invoke(m_guardianValue);
    }
    void Refresh()
    {
        m_guardianValue = new QuestGuardian();
        m_guardianValue.SetWithTransfrom(m_root, m_lineBorder, m_squareArea);
        m_onRefreshed.Invoke(m_guardianValue);
    }

    private void OnValidate()
    {
        if (!Application.isPlaying)
        Refresh();

    }

}
