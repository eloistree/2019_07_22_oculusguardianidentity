﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class QuestGuardianFromOVRMono : MonoBehaviour
{
    public bool m_useDebug;
    public QuestGuardian m_guardianValue;
    [Header("Event")]
    public OnQuestGuaridanChange m_onRefreshed;

    
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        if (OVRManager.boundary.GetConfigured())
            RefreshWithOculusInfo();

    }

    public void Update()
    {
        if (m_useDebug && OVRManager.boundary.GetConfigured() ) {
            Draw_QuestGuardianArea.DebugDrawLine(m_guardianValue, Time.deltaTime);
        }
    }

    public  void RefreshWithOculusInfo()
    {

        m_guardianValue.SetWithLocalPosition( OVRManager.boundary.GetGeometry(OVRBoundary.BoundaryType.OuterBoundary),
 OVRManager.boundary.GetGeometry(OVRBoundary.BoundaryType.PlayArea));
        m_onRefreshed.Invoke(m_guardianValue);
    }

    
}



