﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DisplayGuardianInfo : MonoBehaviour
{
    public Text m_debugText;
    public bool m_id=true;
    public void Refresh(QuestGuardian guardian) {
        if(m_id && m_debugText)
            m_debugText.text = guardian.GetId().GetIdAsString();
    }

}
