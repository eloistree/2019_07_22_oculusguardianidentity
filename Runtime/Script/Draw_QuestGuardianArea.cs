﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.VR;

public class Draw_QuestGuardianArea : MonoBehaviour
{

    [Header("Anchor")]
    public Transform m_whereToDraw;
    public LineRenderer m_lineRenderer;

    [Header("Prefab")]
    public string m_pointsName = "Anchor";
    public GameObject m_anchorPointprefab;

    [Header("Debug")]
    public List<GameObject> m_createdAnchorPoints;


    public void Draw(QuestGuardian guardianInfo)
    {
        if (Application.isPlaying) {

            CreateGuardian(m_whereToDraw, m_anchorPointprefab, guardianInfo.m_border.m_points, out m_createdAnchorPoints, m_pointsName);
            UpdatePosition(m_whereToDraw, guardianInfo.m_border.m_points, m_lineRenderer);
        }
    }

    public static void DebugDrawLine(QuestGuardian guardianValue, float time)
    {

        DebugDrawLine(guardianValue, time, Color.green, Color.yellow);
    }
    public static void DebugDrawLine(QuestGuardian guardianValue, float time, Color borderColor, Color areaColor)
    {
        DebugDrawLine(guardianValue.m_border.m_points, time, borderColor);
        DebugDrawLine(guardianValue.m_playArea.m_points, time, areaColor);
    }
    public static void DebugDrawLine(Vector3[] guardianValue, float time, Color color)
    {
        if (guardianValue.Length > 1) {
            for (int i = 1; i < guardianValue.Length; i++)
            {
                Debug.DrawLine(guardianValue[i - 1], guardianValue[i], color, time);
            }
            Debug.DrawLine(guardianValue[0], guardianValue[guardianValue.Length-1], color, time);
        }
    }

    public static void UpdatePosition(Transform root, LineRenderer renderer)
    {
        UpdatePosition(root, OVRManager.boundary.GetGeometry(OVRBoundary.BoundaryType.OuterBoundary), renderer);
    }

    public static void UpdatePosition(Transform root, Vector3 [] lineBorder, LineRenderer renderer)
    {
       
        if (renderer)
        {
            Vector3[] modifyPosition =new Vector3[ lineBorder.Length + 1];
            renderer.positionCount = modifyPosition.Length;
            for (int i = 0; i < lineBorder.Length; i++)
            {
                modifyPosition[i] = root.TransformPoint(lineBorder[i]);
            }
            modifyPosition[modifyPosition.Length-1] = root.TransformPoint(lineBorder[0]);
            renderer.SetPositions(modifyPosition);
        }
    }

    public static void CreateGuardian(Transform root, GameObject prefab, out List<GameObject> created, string pointName = "Anchor")
    {
        CreateGuardian(root, prefab, OVRManager.boundary.GetGeometry(OVRBoundary.BoundaryType.OuterBoundary), out created, pointName);
    }

        public static void CreateGuardian(Transform root, GameObject prefab, IEnumerable< Vector3> lineBorder, out List<GameObject> created, string pointName="Anchor")
    {
            for (int i = 0; i < root.childCount; i++)
            {
                Destroy(root.GetChild(i).gameObject);
            }
            created = new List<GameObject>();
          
            int index = 0;
            foreach(Vector3 localPosition  in lineBorder)
            {
                GameObject obj = prefab ? GameObject.Instantiate(prefab) : new GameObject();
                obj.name = (index++) + ": " + pointName;
                obj.transform.parent = root;
                obj.transform.localPosition = localPosition;
                obj.transform.rotation = root.rotation;
                created.Add(obj);

            }

    }
 

}
