﻿
using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestGuardian
{
    public BorderLineAsVector m_border;
    public BorderLineAsVector m_playArea;

    public void SetWithLocalPosition(Vector3[] border, Vector3[] playArea)
    {
        RemoveHeightValue(ref border);
        RemoveHeightValue(ref playArea);
        m_border = new BorderLineAsVector(border);
        m_playArea = new BorderLineAsVector(playArea);
    }

    private void RemoveHeightValue(ref Vector3[] value)
    {
        for (int i = 0; i < value.Length; i++)
        {
            value[i].y = 0;
        }
    }

    public void SetWithTransfrom(Transform root, Transform[] borderLine, Transform[] squareArea)
    {
        Vector3[] borderlinePoint = GetLocalPositionFrom(root, borderLine);
        Vector3[] squareAreaPoint = GetLocalPositionFrom(root, squareArea);
        SetWithLocalPosition(borderlinePoint, squareAreaPoint);

    }

    public string ToText()
    {
        string text = "Id:" + GetId().GetIdAsString()+"\n";
        text += "Area\n";
        for (int i = 0; i < this.m_playArea.m_points.Length; i++)
        {
            Vector3 pt = this.m_border.m_points[i];
            text += string.Format("{0}\t{1}\t{2}\n", pt.x, pt.y, pt.z);
        }
        text += "Border\n";
        for (int i = 0; i < this.m_border.m_points.Length; i++)
        { Vector3 pt = this.m_border.m_points[i];
            text += string.Format("{0}\t{1}\t{2}\n", pt.x, pt.y, pt.z);
        }
      
        return text;
    }
    public static QuestGuardian FromText(string text)
    {
        string[] tokens = text.Split('\n');
        int index = 0;
        List<Vector3> border = new List<Vector3>();
        List<Vector3> area = new List<Vector3>();
        string[] values=null; Vector3 pt;
        while (index < tokens.Length )
        {
            values = tokens[index].Split('\t');
            if (values.Length == 3) {
                pt = new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));
                area.Add(pt);
            }
            if (tokens[index].IndexOf("Border") > -1)
                break;
            index++;
        }
        index++;
        while (index < tokens.Length)
        {
            values = tokens[index].Split('\t');
            if (values.Length == 3)
            {
                pt = new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));
                border.Add(pt);
            }
            index++;
        }
        QuestGuardian q = new QuestGuardian();
        q.SetWithLocalPosition(border.ToArray(), area.ToArray());
        return q;
    }

    private Vector3[] GetLocalPositionFrom(Transform m_root, Transform[] inPoints)
    {
        Vector3[] points = new Vector3[inPoints.Length];
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = m_root.InverseTransformPoint(inPoints[i].position);
        }
        return points;
    }

    internal QuestGuardianId GetId()
    {
        return QuestGuardianId.CreateFrom(this);
    }
}


[System.Serializable]
public class BorderLineAsVector
{
    public BorderLineAsVector(Vector3[] points, uint precision=3)
    {
        for (int i = 0; i < points.Length; i++)
        {
            points[i].x = Truncate(points[i].x, precision);
            points[i].y = Truncate(points[i].y, precision);
            points[i].z = Truncate(points[i].z, precision);
        }

        m_points = points;
        m_segmentdistances = new DistanceOfSegmentsInLine(points);
    }

    private static float Truncate(float value, uint  precision)
    {
        float precisionAsDecimal = Mathf.Pow(10f, precision);
        return ((int)(value * precisionAsDecimal)) / precisionAsDecimal;
    }

    public Vector3[] m_points;
    public DistanceOfSegmentsInLine m_segmentdistances;
}

[System.Serializable]
public class DistanceOfSegmentsInLine
{
    public float m_totaleDistance;
    public float m_minSegmentDistance = float.MaxValue;
    public float m_maxSegmentDistance = 0;
    public float[] m_distanceBetweenPoints;
    public DistanceOfSegmentsInLine(Vector3[] points)
    {
        if (points.Length <= 0)
        {
            m_minSegmentDistance = m_maxSegmentDistance = 0f;
            m_distanceBetweenPoints = new float[0];
            return;
        }
        m_distanceBetweenPoints = new float[points.Length - 1];
        for (int i = 0; i < points.Length - 1; i++)
        {
            float dist = Vector3.Distance(points[i], points[i + 1]);
            m_distanceBetweenPoints[i] = dist;
            m_totaleDistance += dist;
            if (m_minSegmentDistance > dist)
                m_minSegmentDistance = dist;
            if (m_maxSegmentDistance < dist)
                m_maxSegmentDistance = dist;

        }
    }
}