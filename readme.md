# How to use: Oculus Guardian Identity   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.oculusguardianidentity":"https://gitlab.com/eloistree/2019_07_22_oculusguardianidentity.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.oculusguardianidentity",                              
  "displayName": "Oculus Guardian Identity",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Tool that allow you to identify a guardian system from Oculus and access a history of them.",                         
  "keywords": ["Script","Tool","init"],                       
  "category": "Script",                   
  "dependencies":{"be.eloiexperiments.randomtool": "0.0.1"}     
  }                                                                                
```    